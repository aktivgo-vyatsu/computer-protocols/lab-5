from ftp.client import FtpClient
from ftp.library_client import LibraryFtpClient
from ftp.socket_client import SocketFtpClient


def pipline(ftp_client: FtpClient, file_name, file_path, out_file_path):
    try:
        print(ftp_client.connect())
        print(ftp_client.login('one', '1234'))
        print(ftp_client.store(file_name, file_path))
        print(ftp_client.retrieve(file_name, out_file_path))
        print(ftp_client.disconnect())
    except Exception as e:
        print(e)


if __name__ == "__main__":
    library_ftp_client = LibraryFtpClient()
    socket_ftp_client = SocketFtpClient()

    pipline(library_ftp_client, 'my_file.txt', 'ftp/files/my_file.txt', 'ftp/files/received_by_library_my_file.txt')
    pipline(socket_ftp_client, 'my_file.txt', 'ftp/files/my_file.txt', 'ftp/files/received_by_socket_my_file.txt')
