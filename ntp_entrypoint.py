from ntp.library_client import LibraryNtpClient
from time import ctime
from datetime import datetime

if __name__ == "__main__":
    library_ntp_client = LibraryNtpClient()

    response = library_ntp_client.request('localhost', '123')
    print(f'Time on ntp server: {response.tx_time}s or {ctime(response.tx_time)}')

    now_seconds = datetime.now().timestamp()
    print(f'Time on this pc: {now_seconds}s or {ctime(now_seconds)}')

    seconds_diff = abs(now_seconds - response.tx_time)
    print(f'Time difference with server: {seconds_diff}s')

    seconds = int(seconds_diff)
    milliseconds = int((seconds_diff - seconds) * 1000)
    nanoseconds = int((seconds_diff - milliseconds / 1000) * 1000000000)
    print(f'Time difference with server: {seconds}s {milliseconds}ms {nanoseconds}ns')
