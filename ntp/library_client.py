import ntplib


class LibraryNtpClient:
    def __init__(self):
        self.client = ntplib.NTPClient()

    def request(self, host='localhost', port='123'):
        return self.client.request(host=host, port=port)