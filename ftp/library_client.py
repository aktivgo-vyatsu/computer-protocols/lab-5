from .client import FtpClient
from ftplib import FTP_TLS


class LibraryFtpClient(FtpClient):
    def __init__(self, host='localhost', port=21):
        self.host = host
        self.port = port
        self.client = FTP_TLS()

    def connect(self):
        return self.client.connect(self.host, self.port)

    def login(self, login='anonymous', password='anonymous'):
        self.client.sendcmd('USER one')
        return self.client.sendcmd('PASS 1234')

    def store(self, file_name, file_path):
        with open(file_path, 'rb') as f:
            return self.client.storlines('STOR ' + file_name, f)

    def retrieve(self, file_name, out_file_path):
        with open(out_file_path, 'w') as f:
            return self.client.retrlines('RETR ' + file_name, f.write)

    def disconnect(self):
        self.client.close()
        return 'disconnected'
