from .client import FtpClient
from socket import *
import re

BUFFER_SIZE = 1024


class SocketFtpClient(FtpClient):
    def __init__(self, host='localhost', port=21):
        self.host = host
        self.port = port
        self.client = socket(AF_INET, SOCK_STREAM)

    def connect(self):
        self.client.connect((self.host, self.port))
        return self.client.recv(BUFFER_SIZE).decode()

    def login(self, login='anonymous', password='anonymous'):
        self.client.send(f'USER {login}\r\n'.encode())
        user_result = self.client.recv(BUFFER_SIZE).decode()
        if not user_result.startswith('331'):
            raise Exception(user_result)

        self.client.send(f'PASS {password}\r\n'.encode())
        pass_result = self.client.recv(BUFFER_SIZE).decode()
        if not pass_result.startswith('230'):
            raise Exception(pass_result)

        return pass_result

    def store(self, file_name, file_path):
        passive_client = self.__get_passive_client()

        self.client.send(f'STOR {file_name}\r\n'.encode())
        stor_request_result = self.client.recv(BUFFER_SIZE).decode()
        if not stor_request_result.startswith('150'):
            raise ValueError(stor_request_result)

        with open(file_path, 'rb') as file:
            passive_client.sendfile(file)
        passive_client.close()

        stor_result = self.client.recv(BUFFER_SIZE).decode()
        if not stor_result.startswith('226'):
            raise ValueError(stor_result)

        return stor_result

    def retrieve(self, filename, out_file_path):
        passive_client = self.__get_passive_client()

        self.client.send(f'RETR {filename}\r\n'.encode())
        retr_request_result = self.client.recv(BUFFER_SIZE).decode()
        if not retr_request_result.startswith('150'):
            raise Exception(retr_request_result)

        with open(out_file_path, 'wb') as file:
            file.write(passive_client.recv(BUFFER_SIZE))
        passive_client.close()

        retr_result = self.client.recv(BUFFER_SIZE).decode()
        if not retr_result.startswith('226'):
            raise Exception(retr_result)

        return retr_result

    def __get_passive_client(self):
        self.client.send('PASV\r\n'.encode())
        result = self.client.recv(BUFFER_SIZE).decode()
        if not result.startswith('227'):
            raise Exception(result)

        address = re.split('[()]', result)[1].split(',')
        passive_client = socket(AF_INET, SOCK_STREAM)
        passive_client.connect(('127.0.0.1', int(address[4]) * 256 + int(address[5])))
        return passive_client

    def disconnect(self):
        self.client.send('QUIT\r\n'.encode())
        return self.client.recv(BUFFER_SIZE).decode()
